"""
turnin.py - Use this file to enter your answers, and turn in a printout of this file.

UVA CS 1120
Spring 2016

Project 1 -- Making Mosaics

My name:    [replace with your name]
My UVA ID:  [replace with your ID]
"""

# The next line includes the provided code for use in your answers: (don't delete it)
from mosaic import *  

# Question 1 - enter your predictions here (as Python comments, or attach a separate printed sheet)

# a. 1120

# b. 70 + 80

# c. +

# d. 3 > 2

# e. (3 > 2) and (4 > 5)

# f. 
#    if (12 > 10):
#       "good move"
#    else:
#       "try again"

# g.
#    if not "cookies":
#       "eat"
#    else:
#       "starve"

# h. if (10 > 11): +

# Question 2 - use the Python interpreter to try out the program
# fragments, and explain the difference in comments here.  Note that
# whitespace matters in Python, so you will need to be careful to match the
# intendatation as given.

# Question 3 - write your code to answer the question below


# Question 4 - write your code to answer the question below


# Question 5 - write your code to answer the question below

def closer_color(sample, color1, color2):
    """
    Put your code here that implements the closer_color function
    Your function should return True if color1 is closer to sample,
    and otherwise return False.
    """
    return False


# Uncomment the next line once you've attempted Question 5 to
# make a photomosaic.  To make something better, replace the provided
# rotunda.gif with a more interesting picture!  (Must be in .gif format.)

# make_photomosaic("rotunda.gif", "tiles/", closer_color)
